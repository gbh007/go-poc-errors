package main

import (
	"database/sql"
	"log"
	"time"
)

type Result[T any] struct {
	Out      T
	Error    error
	ToAsync  bool
	GetAfter chan Result[T]
}

type work[T any] func() (T, error)

func handle[T any](w work[T]) Result[T] {
	resultChan := make(chan Result[T])

	go func() {
		out, err := w()
		resultChan <- Result[T]{
			Out:     out,
			Error:   err,
			ToAsync: false,
		}
	}()

	select {
	case result := <-resultChan:
		return result
	case <-time.NewTimer(time.Second).C:
		return Result[T]{
			ToAsync:  true,
			GetAfter: resultChan,
		}
	}
}

func test1() (int, error) {
	return 0, nil
}

func test2() (string, error) {
	time.Sleep(time.Second * 2)

	return "some", nil
}

func test3() (string, error) {

	return "", sql.ErrNoRows
}

func printer[T any](name string, w work[T]) {
	log.Printf("start: %s\n", name)
	result := handle(w)
	if !result.ToAsync {
		if result.Error != nil {
			log.Printf("sync failed: %s - %s\n", name, result.Error.Error())
		} else {
			log.Printf("sync success: %s - %v\n", name, result.Out)
		}
	} else {
		log.Printf("go to async: %s\n", name)
		go func() {
			result = <-result.GetAfter
			if result.Error != nil {
				log.Printf("async failed: %s - %s\n", name, result.Error.Error())
			} else {
				log.Printf("async success: %s - %v\n", name, result.Out)
			}
		}()
	}
}

func main() {
	printer("test1", test1)
	printer("test2", test2)
	printer("test3", test3)
	time.Sleep(time.Second * 3)
}
