package main

import (
	"app/database/pgxtablescanner/entities"
	"app/database/pgxtablescanner/table"
	"context"

	"github.com/Masterminds/squirrel"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

func main() {}

func BookPagesWithHash(ctx context.Context, bookID uuid.UUID) ([]entities.PageWithHash, error) {
	out, err := table.PageWithHashTable.SelectBy(
		ctx,
		&pgxpool.Pool{},
		func(builder squirrel.SelectBuilder) squirrel.SelectBuilder {
			return builder.Where(squirrel.Eq{
				"p.book_id": bookID,
			}).OrderBy("p.page_number")
		},
		func(ctx context.Context, query string, args []any) {},
	)
	if err != nil {
		return nil, err
	}

	return out, nil
}

func BookPageWithHash(ctx context.Context, bookID uuid.UUID, pageNumber int) (entities.PageWithHash, error) {
	page, err := table.PageWithHashTable.SelectOneBy(
		ctx,
		&pgxpool.Pool{},
		func(builder squirrel.SelectBuilder) squirrel.SelectBuilder {
			return builder.Where(squirrel.Eq{
				"p.book_id":     bookID,
				"p.page_number": pageNumber,
			}).Limit(1)
		},
		func(ctx context.Context, query string, args []any) {},
	)
	if err != nil {
		return entities.PageWithHash{}, err
	}

	return page, nil
}

func BookPagesWithHashByHash(ctx context.Context, hash entities.FileHash) ([]entities.PageWithHash, error) {
	out, err := table.PageWithHashTable.SelectBy(
		ctx,
		&pgxpool.Pool{},
		func(builder squirrel.SelectBuilder) squirrel.SelectBuilder {
			return builder.Where(squirrel.Eq{
				"f.md5_sum":    hash.Md5Sum,
				"f.sha256_sum": hash.Sha256Sum,
				"f.size":       hash.Size,
			})
		},
		func(ctx context.Context, query string, args []any) {},
	)
	if err != nil {
		return nil, err
	}

	return out, nil
}

func BookPagesCountByHash(ctx context.Context, hash entities.FileHash) (int64, error) {
	count, err := table.PageWithHashTable.CountOneBy(
		ctx,
		&pgxpool.Pool{},
		func(builder squirrel.SelectBuilder) squirrel.SelectBuilder {
			return builder.Where(squirrel.Eq{
				"f.md5_sum":    hash.Md5Sum,
				"f.sha256_sum": hash.Sha256Sum,
				"f.size":       hash.Size,
			})
		},
		func(ctx context.Context, query string, args []any) {},
	)
	if err != nil {
		return 0, err
	}

	return count, nil
}
