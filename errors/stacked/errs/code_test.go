package errs

import (
	"database/sql"
	"errors"
	"fmt"
	"testing"
)

func TestCodeData(t *testing.T) {
	code1 := &Code{
		err:   sql.ErrNoRows,
		Code:  "1",
		Title: "title-1",
		Text:  "text-1",
	}
	code2 := &Code{
		err:   sql.ErrNoRows,
		Code:  "2",
		Title: "title-2",
		Text:  "text-2",
	}
	code1eq := &Code{
		err:   sql.ErrConnDone,
		Code:  "1",
		Title: "title-1-eq",
		Text:  "text-1-eq",
	}

	// Разные коды с одинаковой внутренней ошибкой не должны быть равны
	if errors.Is(code1, code2) {
		t.Fatal(`errors.Is(code1, code2)`)
	}

	// Одинаковый код вне зависимости от прочих должен быть эквивалентен
	if !errors.Is(code1, code1eq) {
		t.Fatal(`!errors.Is(code1, code1eq)`)
	}

	someWrapped1 := fmt.Errorf("wrapped: %w", code1)

	// Одинаковый код обернутый в ошибку
	if !errors.Is(someWrapped1, code1eq) {
		t.Fatal(`!errors.Is(someWrapped1, code1eq)`)
	}

	// Вложенная в код ошибка
	if !errors.Is(someWrapped1, sql.ErrNoRows) {
		t.Fatal(`!errors.Is(someWrapped1, sql.ErrNoRows)`)
	}
}
