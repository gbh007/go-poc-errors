package errs

import (
	"errors"
	"fmt"
	"runtime"
)

type ErrorTraceUnit struct {
	From   string
	Method string
}

type Trace struct {
	// обернутая ошибка
	err error
	// трассировка
	Trace []ErrorTraceUnit
}

func (td *Trace) Error() string { return td.err.Error() }
func (td *Trace) Unwrap() error { return td.err }

func (td *Trace) Is(err error) bool {
	target := &Trace{}

	// это другой трейс, проверяем обернутую ошибку
	if errors.As(err, &target) {
		return errors.Is(td.err, target.err)
	}

	return errors.Is(td.err, err)
}

func StackTrace(skip int) []ErrorTraceUnit {
	pc := make([]uintptr, 50)
	n := runtime.Callers(skip, pc)
	pc = pc[:n]

	frames := runtime.CallersFrames(pc)
	units := make([]ErrorTraceUnit, 0, n)

	for {
		frame, more := frames.Next()

		units = append(units, ErrorTraceUnit{
			From:   fmt.Sprintf("%s:%d", frame.File, frame.Line),
			Method: frame.Function,
		})

		if !more {
			break
		}
	}

	return units
}
