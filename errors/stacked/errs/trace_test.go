package errs

import (
	"database/sql"
	"errors"
	"testing"
)

func TestTraceData(t *testing.T) {
	trace1 := &Trace{
		err:   sql.ErrNoRows,
		Trace: []ErrorTraceUnit{},
	}
	trace2 := &Trace{
		err:   sql.ErrConnDone,
		Trace: []ErrorTraceUnit{},
	}
	trace1eq := &Trace{
		err:   sql.ErrNoRows,
		Trace: []ErrorTraceUnit{},
	}

	// Трейсы с разными ошибками внутри не эквивалентны
	if errors.Is(trace1, trace2) {
		t.Fatal(`errors.Is(trace1, trace2)`)
	}

	// Трейсы с одинаковыми ошибками внутри эквивалентны
	if !errors.Is(trace1, trace1eq) {
		t.Fatal(`!errors.Is(trace1, trace1eq)`)
	}

	// Трейс проверяется на вложенную ошибку
	if !errors.Is(trace1, sql.ErrNoRows) {
		t.Fatal(`!errors.Is(trace1, sql.ErrNoRows)`)
	}
}
