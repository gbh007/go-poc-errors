package errs

type Code interface {
	error

	DisplayedCode() int
}

type errorWithCode struct {
	error

	code int
}

func WithDisplayedCode(err error, code int) error {
	return &errorWithCode{
		error: err,
		code:  code,
	}
}

func (e *errorWithCode) Unwrap() error {
	return e.error
}

func (e *errorWithCode) DisplayedCode() int {
	return e.code
}
