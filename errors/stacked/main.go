package main

import (
	"app/errors/stacked/errs"
	"database/sql"
	"errors"
	"fmt"
	"strings"
)

func main() {
	newStackExample()
	newStackWithCodeExample()
}

func newStackWithCodeExample() {
	err1 := errs.NewStackWithCode(sql.ErrNoRows, errs.ErrCode2)
	showErrorInfo(err1)

	err2 := errs.NewStackWithCode(err1, errs.ErrCode3)
	showErrorInfo(err2)
}
func newStackExample() {
	err1 := errs.NewStack(sql.ErrNoRows)
	showErrorInfo(err1)

	err2 := errs.NewStack(err1)
	showErrorInfo(err2)
}

func showErrorInfo(err error) {
	fmt.Println(strings.Repeat("#", 20))
	fmt.Printf("error >>> %s\n", err)

	codeInfo := new(errs.Code)

	if errors.As(err, &codeInfo) {
		fmt.Printf("code >>> %s\n", codeInfo.Code)
		fmt.Printf("title >>> %s\n", codeInfo.Title)
		fmt.Printf("text >>> %s\n", codeInfo.Text)
	}

	traceInfo := new(errs.Trace)

	if errors.As(err, &traceInfo) {
		fmt.Printf("Trace:\n")
		for index, value := range traceInfo.Trace {
			fmt.Printf("=== (%d) === %s > %s\n", index, value.From, value.Method)
		}
	}
}
