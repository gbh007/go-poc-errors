package main

import (
	"app/errors/genericwrap/errs"
	"database/sql"
	"errors"
	"fmt"
	"strings"
)

func main() {
	newStackExample()
	newStackWithCodeExample()
}

func newStackWithCodeExample() {
	err1 := errs.NewStackWithCode(sql.ErrNoRows, 2)
	showErrorInfo(err1)

	err2 := errs.NewStackWithCode(err1, 3)
	showErrorInfo(err2)
}
func newStackExample() {
	err1 := errs.NewStack(sql.ErrNoRows)
	showErrorInfo(err1)

	err2 := errs.NewStack(err1)
	showErrorInfo(err2)
}

func showErrorInfo(err error) {
	fmt.Println(strings.Repeat("#", 20))
	fmt.Printf("error >>> %s\n", err)

	var (
		codeInfo errs.Code
	)

	if errors.As(err, &codeInfo) {
		fmt.Printf("code >>> %d\n", codeInfo.DisplayedCode())
	}

	var traceInfo errs.Trace

	if errors.As(err, &traceInfo) {
		fmt.Printf("Trace:\n%s\n", traceInfo.Trace())
	}
}
