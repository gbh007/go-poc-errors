package errs

import (
	"database/sql"
	"errors"
	"testing"
)

func TestTextData(t *testing.T) {
	const someText = "Hello world!"
	code1 := WithDisplayedText(sql.ErrNoRows, someText)

	if !errors.Is(code1, sql.ErrNoRows) {
		t.Fail()
	}

	var code1Unwrap Text

	if !errors.As(code1, &code1Unwrap) {
		t.FailNow()
	}

	if code1Unwrap.DisplayedText() != someText {
		t.Fail()
	}
}
