package errs

import (
	"errors"
)

type Code struct {
	// Ошибка которую обернули в код
	err error
	// Некий уникальный код ошибки
	Code string
	// Заголовок ошибки (текст на кириллице)
	Title string
	// Описание ошибки (текст на кириллице)
	Text string
}

func (cd *Code) Error() string { return cd.err.Error() }
func (cd *Code) Unwrap() error { return cd.err }

// При сравнении кодов учитывается только последний код, эквивалентность внутренних ошибок не важна
func (cd *Code) Is(err error) bool {
	target := new(Code)

	// Сравниваем код ошибки с кодом ошибки
	if errors.As(err, &target) {
		return cd.Code == target.Code
	}

	return errors.Is(cd.err, err)
}
