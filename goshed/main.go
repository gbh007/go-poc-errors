package main

import (
	"fmt"
	"runtime"
	"sync"
	"time"
)

func main() {
	runtime.GOMAXPROCS(1)

	wg := new(sync.WaitGroup)

	l := 1000000
	goshedDur := time.Duration(0)
	noGoshedDur := time.Duration(0)

	for i := 0; i < l; i++ {
		wg.Add(1)

		start := time.Now()
		go func() {
			wg.Done()
		}()

		runtime.Gosched()
		wg.Wait()
		goshedDur += time.Since(start)
	}

	for i := 0; i < l; i++ {
		wg.Add(1)

		start := time.Now()
		go func() {
			wg.Done()
		}()

		wg.Wait()
		noGoshedDur += time.Since(start)
	}

	fmt.Println("avg diff", (noGoshedDur-goshedDur)/time.Duration(l))
	fmt.Printf("%.2f%%\n", float64(noGoshedDur-goshedDur)*100.0/float64(noGoshedDur))
}
