package memory

import "testing"

type Some struct {
	pointer *int
}

func BenchmarkNewInit(b *testing.B) {
	for i := 0; i < b.N; i++ {
		var value int
		s := new(Some)
		s.pointer = &value
	}
}

func BenchmarkPointerInit(b *testing.B) {
	for i := 0; i < b.N; i++ {
		var value int
		s := &Some{
			pointer: &value,
		}
		_ = s
	}
}
func BenchmarkPointerAfterInit(b *testing.B) {
	for i := 0; i < b.N; i++ {
		var value int
		s := &Some{}
		s.pointer = &value
	}
}

func BenchmarkSimpleInit(b *testing.B) {
	for i := 0; i < b.N; i++ {
		var value int
		s := Some{
			pointer: &value,
		}
		_ = s
	}
}

func BenchmarkSimpleAfterInit(b *testing.B) {
	for i := 0; i < b.N; i++ {
		var value int
		s := Some{}
		s.pointer = &value
	}
}
