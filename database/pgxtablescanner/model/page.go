package model

import (
	"app/database/pgxtablescanner/entities"
	"database/sql"
	"net/url"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

type PageWithHash struct {
	BookID     uuid.UUID      `db:"book_id"`
	PageNumber int            `db:"page_number"`
	Ext        string         `db:"ext"`
	OriginURL  sql.NullString `db:"origin_url"`
	Downloaded bool           `db:"downloaded"`
	FileID     sql.NullString `db:"file_id"`
	Md5Sum     sql.NullString `db:"md5_sum"`
	Sha256Sum  sql.NullString `db:"sha256_sum"`
	Size       sql.NullInt64  `db:"size"`
}

func (p PageWithHash) ToEntity() (entities.PageWithHash, error) {
	var (
		originURL *url.URL
		err       error
	)

	if p.OriginURL.Valid {
		originURL, err = url.Parse(p.OriginURL.String)
		if err != nil {
			return entities.PageWithHash{}, err
		}
	}

	fileID := uuid.Nil

	if p.FileID.Valid {
		fileID, err = uuid.Parse(p.FileID.String)
		if err != nil {
			return entities.PageWithHash{}, err
		}
	}

	return entities.PageWithHash{
		BookID:     p.BookID,
		PageNumber: p.PageNumber,
		Ext:        p.Ext,
		OriginURL:  originURL,
		Downloaded: p.Downloaded,
		FileID:     fileID,
		Md5Sum:     p.Md5Sum.String,
		Sha256Sum:  p.Sha256Sum.String,
		Size:       p.Size.Int64,
	}, nil
}

func (p *PageWithHash) ScanRow(rows pgx.Rows) error {
	return rows.Scan(
		&p.BookID,
		&p.PageNumber,
		&p.Ext,
		&p.OriginURL,
		&p.Downloaded,
		&p.FileID,
		&p.Md5Sum,
		&p.Sha256Sum,
		&p.Size,
	)
}
