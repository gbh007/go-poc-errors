package errs

import (
	"errors"
	"fmt"
)

func NewStack(err error) error {
	if err == nil {
		return nil
	}

	err = withTrace(err)

	var codeInfo Code

	// нет данных кода
	if !errors.As(err, &codeInfo) {
		err = WithDisplayedCode(err, 1)
	}

	return err
}

func NewStackWithCode(err error, codeError int) error {
	if err == nil {
		return nil
	}

	err = withTrace(err)

	err = WithDisplayedCode(err, codeError)

	return err
}

func NewStackWithCodeF(codeError int, format string, args ...interface{}) error {
	err := fmt.Errorf(format, args...)
	err = withTrace(err)

	return NewStackWithCode(err, codeError)
}

func Newf(format string, args ...interface{}) error {
	err := fmt.Errorf(format, args...)
	err = withTrace(err)

	return NewStack(err)
}

func withTrace(err error) error {
	if err == nil {
		return nil
	}

	var traceInfo Trace

	// нет данных трассировки
	if !errors.As(err, &traceInfo) {
		err = WithTrace(err, 4)
	}

	return err
}
