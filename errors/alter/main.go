package main

import (
	"encoding/json"
	"errors"
	"fmt"
)

type toM struct {
	Code      string     `json:"code,omitempty"`
	OldCodes  []string   `json:"old_codes,omitempty"`
	Trace     []string   `json:"trace,omitempty"`
	OldTraces [][]string `json:"old_traces,omitempty"`
	Text      string     `json:"text,omitempty"`
	OldTexts  []string   `json:"old_texts,omitempty"`
	Error     string     `json:"error"`
	OldErrors []string   `json:"old_errors,omitempty"`
}

func superMarshal(errData error) ([]byte, error) {
	data := new(toM)

	for {
		if errData == nil {
			break
		}
		switch sw := errData.(type) {

		case Code:
			if data.Code != "" {
				data.OldCodes = append(data.OldCodes, sw.Code())
			} else {
				data.Code = sw.Code()
			}

		case Trace:
			if len(data.Trace) > 0 {
				data.OldTraces = append(data.OldTraces, sw.Trace())
			} else {
				data.Trace = sw.Trace()
			}

		case Text:
			if data.Text != "" {
				data.OldTexts = append(data.OldTexts, sw.Text())
			} else {
				data.Text = sw.Text()
			}

		default:
			if data.Error != "" {
				data.OldErrors = append(data.OldErrors, errData.Error())
			} else {
				data.Error = errData.Error()
			}

		}

		errData = errors.Unwrap(errData)
	}

	return json.MarshalIndent(data, "", "    ")
}

type Code interface {
	error
	Code() string
}

func WithCode(err error, code string) error {
	return codeImpl{
		error: err,
		code:  code,
	}
}

type codeImpl struct {
	error
	code string
}

func (c codeImpl) Code() string                 { return c.code }
func (c codeImpl) MarshalJSON() ([]byte, error) { return superMarshal(c) }
func (c codeImpl) Unwrap() error                { return c.error }

type Trace interface {
	error
	Trace() []string
}

func WithTrace(err error, trace []string) error {
	return traceImpl{
		error: err,
		trace: trace,
	}
}

type traceImpl struct {
	error
	trace []string
}

func (c traceImpl) Trace() []string              { return c.trace }
func (c traceImpl) MarshalJSON() ([]byte, error) { return superMarshal(c) }
func (c traceImpl) Unwrap() error                { return c.error }

type Text interface {
	error
	Text() string
}

func WithText(err error, text string) error {
	return textImpl{
		error: err,
		text:  text,
	}
}

type textImpl struct {
	error
	text string
}

func (c textImpl) Text() string                 { return c.text }
func (c textImpl) MarshalJSON() ([]byte, error) { return superMarshal(c) }
func (c textImpl) Unwrap() error                { return c.error }
func (c textImpl) String() string {
	data, err := superMarshal(c)
	if err != nil {
		return c.error.Error()
	}

	return string(data)
}

func main() {
	err := errors.New("some error in code")
	err = WithCode(err, "123")
	err = WithTrace(err, []string{"01", "02", "03"})
	err = WithText(err, "Текст для пользователя")

	err = WithCode(err, "456")
	err = WithTrace(err, []string{"7", "8", "9"})
	err = WithText(err, "Новинка для пользователя")

	fmt.Println(err)
	data, _ := json.MarshalIndent(err, "", "    ")
	fmt.Println(string(data))
}
