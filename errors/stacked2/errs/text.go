package errs

type Text interface {
	error

	DisplayedText() string
}

type errorWithText struct {
	error

	text string
}

func WithDisplayedText(err error, text string) error {
	return &errorWithText{
		error: err,
		text:  text,
	}
}

func (e *errorWithText) Unwrap() error {
	return e.error
}

func (e *errorWithText) DisplayedText() string {
	return e.text
}
