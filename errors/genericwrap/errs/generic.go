package errs

type superWrapper[T any] struct {
	error

	a T
}

func SuperWrap[T any](err error, a T) error {
	return &superWrapper[T]{
		error: err,
		a:     a,
	}
}

func (ewt *superWrapper[T]) Unwrap() error {
	return ewt.error
}

func (ewt *superWrapper[T]) As(target any) bool {
	switch v := target.(type) {
	case *T:
		*v = ewt.a

		return true
	}

	return false
}

type as interface{ As(any) bool }

var _ as = &superWrapper[struct{}]{}
