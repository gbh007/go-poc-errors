package errs

var codeList []Code

func CodeList() []Code {
	list := make([]Code, len(codeList))

	copy(list, codeList)

	return list
}

func addCode(code, title, text string) Code {
	c := Code{
		Code:  code,
		Title: title,
		Text:  text,
	}

	codeList = append(codeList, c)

	return c
}

var (
	ErrCode1 = addCode("1", "added code 1 title", "added code 1 text")
	ErrCode2 = addCode("2", "added code 2 title", "added code 2 text")
	ErrCode3 = addCode("3", "added code 3 title", "added code 3 text")
)
