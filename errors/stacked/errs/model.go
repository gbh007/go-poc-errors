package errs

import (
	"errors"
	"fmt"
)

func NewStack(err error) error {
	if err == nil {
		return nil
	}

	err = withTrace(err)

	codeInfo := new(Code)

	// нет данных кода
	if !errors.As(err, &codeInfo) {
		err = &Code{
			err:   err,
			Code:  "1",
			Title: "default title",
			Text:  "default text",
		}
	}

	return err
}

func NewStackWithCode(err error, codeError Code) error {
	if err == nil {
		return nil
	}

	err = withTrace(err)

	oldCodeInfo := new(Code)

	// есть старый код
	if errors.As(err, &oldCodeInfo) {
		err = fmt.Errorf("old code - %s detected: %w", oldCodeInfo.Code, err)
	}

	err = &Code{
		err:   err,
		Code:  codeError.Code,
		Title: codeError.Title,
		Text:  codeError.Text,
	}

	return err
}

func NewStackWithCodeF(codeError Code, format string, args ...interface{}) error {
	err := fmt.Errorf(format, args...)
	err = withTrace(err)

	return NewStackWithCode(err, codeError)
}

func Newf(format string, args ...interface{}) error {
	err := fmt.Errorf(format, args...)
	err = withTrace(err)

	return NewStack(err)
}

func withTrace(err error) error {
	if err == nil {
		return nil
	}

	traceInfo := new(Trace)

	// нет данных трассировки
	if !errors.As(err, &traceInfo) {
		err = &Trace{
			err:   err,
			Trace: StackTrace(4),
		}
	}

	return err
}
