package errs

import (
	"database/sql"
	"errors"
	"fmt"
	"testing"
)

func TestCodeData(t *testing.T) {
	code1 := WithDisplayedCode(sql.ErrNoRows, 1)
	code2 := WithDisplayedCode(sql.ErrNoRows, 2)
	code2wrap := WithDisplayedCode(fmt.Errorf("some bad: %w", sql.ErrNoRows), 2)
	code1eq := WithDisplayedCode(sql.ErrConnDone, 1)
	code3 := WithDisplayedCode(code2wrap, 3)

	if errors.Is(code1, sql.ErrConnDone) {
		t.Fail()
	}

	if !errors.Is(code1, sql.ErrNoRows) {
		t.Fail()
	}

	if !errors.Is(code2wrap, sql.ErrNoRows) {
		t.Fail()
	}

	var code1Unwrap,
		code2Unwrap,
		code1eqUnwrap,
		code3Unwrap Code

	if !errors.As(code1, &code1Unwrap) {
		t.FailNow()
	}

	if !errors.As(code2, &code2Unwrap) {
		t.FailNow()
	}

	if !errors.As(code1eq, &code1eqUnwrap) {
		t.FailNow()
	}

	if !errors.As(code3, &code3Unwrap) {
		t.FailNow()
	}

	if code1Unwrap.DisplayedCode() != code1eqUnwrap.DisplayedCode() {
		t.Fail()
	}

	if code1Unwrap.DisplayedCode() == code2Unwrap.DisplayedCode() {
		t.Fail()
	}

	if code1Unwrap.DisplayedCode() != 1 {
		t.Fail()
	}

	if code2Unwrap.DisplayedCode() != 2 {
		t.Fail()
	}

	if code3Unwrap.DisplayedCode() != 3 {
		t.Fail()
	}
}
