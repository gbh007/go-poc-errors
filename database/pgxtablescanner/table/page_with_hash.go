package table

import (
	"app/database/pgxtablescanner/entities"
	"app/database/pgxtablescanner/model"
	"fmt"

	"github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v5"
)

var PageWithHashTable = NewTable(
	[]string{
		"p.book_id",
		"p.page_number",
		"p.ext",
		"p.origin_url",
		"p.downloaded",
		"p.file_id",
		"f.md5_sum",
		"f.sha256_sum",
		"f.size",
	},
	func(data *entities.PageWithHash) RowScanner {
		return func(rows pgx.Rows) error {
			raw := model.PageWithHash{}
			err := raw.ScanRow(rows)
			if err != nil {
				return fmt.Errorf("scan to model: %w", err)
			}

			*data, err = raw.ToEntity()
			if err != nil {
				return fmt.Errorf("convert to entity: %w", err)
			}

			return nil
		}
	},
	func(builder squirrel.SelectBuilder) squirrel.SelectBuilder {
		return builder.From("pages p").
			LeftJoin("files f ON p.file_id = f.id")
	},
)
