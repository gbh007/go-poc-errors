package errs

type Code interface {
	DisplayedCode() int
}

type errorCode struct {
	code int
}

func WithDisplayedCode(err error, code int) error {
	return SuperWrap[Code](err, &errorCode{
		code: code,
	})
}

func (e *errorCode) DisplayedCode() int {
	return e.code
}
