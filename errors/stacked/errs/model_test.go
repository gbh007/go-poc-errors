package errs

import (
	"database/sql"
	"errors"
	"fmt"
	"testing"
)

func TestNewStack(t *testing.T) {
	t.Parallel()

	// Проверка на пустую ошибку (nil)
	if NewStack(nil) != nil {
		t.Fatal(`NewStack(nil) != nil`)
	}

	err := NewStack(sql.ErrNoRows)

	// Проверка на сравнение с чистой ошибкой
	if !errors.Is(err, sql.ErrNoRows) {
		t.Fatal(`!errors.Is(NewStack(err, sql.ErrNoRows)`)
	}

	wrappedError := NewStack(sql.ErrNoRows)

	// Проверка на сравнение с обернутой ошибкой
	if !errors.Is(err, wrappedError) {
		t.Fatal(`!errors.Is(NewStack(err, wrappedError)`)
	}

	err = NewStack(err)

	// Проверка на сравнение с чистой ошибкой после переобертки
	if !errors.Is(err, sql.ErrNoRows) {
		t.Fatal(`!errors.Is(NewStack(err, sql.ErrNoRows)`)
	}
}

func TestNewStackWithCode(t *testing.T) {
	t.Parallel()

	// Проверка на пустую ошибку (nil)
	if NewStackWithCode(nil, ErrCode1) != nil {
		t.Fatal(`NewStackWithCode(nil) != nil`)
	}

	err := NewStackWithCode(sql.ErrNoRows, ErrCode1)

	// Проверка на сравнение с чистой ошибкой
	if !errors.Is(err, sql.ErrNoRows) {
		t.Fatal(`!errors.Is(err, sql.ErrNoRows)`)
	}

	// Проверка на сравнение с кодом (mvnoErrors)
	if !errors.Is(err, &ErrCode1) {
		t.Fatal(`!errors.Is(err, ErrCode1)`)
	}

	err = NewStackWithCode(err, ErrCode2)

	// Проверка на сравнение с чистой ошибкой после переобертки
	if !errors.Is(err, sql.ErrNoRows) {
		t.Fatal(`!errors.Is(err, sql.ErrNoRows)`)
	}

	// Проверка на сравнение с кодом (mvnoErrors) после переобертки
	if !errors.Is(err, &ErrCode2) {
		t.Fatal(`!errors.Is(err, ErrCode2)`)
	}
}

func TestSimpleError(t *testing.T) {
	t.Parallel()

	err := fmt.Errorf("db error: %w", sql.ErrNoRows)

	// Проверка на сравнение с чистой ошибкой
	if !errors.Is(err, sql.ErrNoRows) {
		t.Fatal(`!errors.Is(err, sql.ErrNoRows)`)
	}
}

func TestNewStackWithCodeF(t *testing.T) {
	t.Parallel()

	err := NewStackWithCodeF(ErrCode1, "db error: %w", sql.ErrNoRows)

	// Проверка на сравнение с чистой ошибкой
	if !errors.Is(err, sql.ErrNoRows) {
		t.Fatal(`!errors.Is(err, sql.ErrNoRows)`)
	}

	// Проверка на сравнение с кодом (mvnoErrors)
	if !errors.Is(err, &ErrCode1) {
		t.Fatal(`!errors.Is(err, ErrCode1)`)
	}

	err = NewStackWithCodeF(ErrCode2, "origin error: %w", err)

	// Проверка на сравнение с чистой ошибкой после переобертки
	if !errors.Is(err, sql.ErrNoRows) {
		t.Fatal(`!errors.Is(err, sql.ErrNoRows)`)
	}

	// Проверка на сравнение с кодом (mvnoErrors) после переобертки
	if !errors.Is(err, &ErrCode2) {
		t.Fatal(`!errors.Is(err, ErrCode2)`)
	}
}

func TestNewf(t *testing.T) {
	t.Parallel()

	err := Newf("db error: %w", sql.ErrNoRows)

	// Проверка на сравнение с чистой ошибкой
	if !errors.Is(err, sql.ErrNoRows) {
		t.Fatal(`!errors.Is(err, sql.ErrNoRows)`)
	}

	err = Newf("origin error: %w", err)

	// Проверка на сравнение с чистой ошибкой после переобертки
	if !errors.Is(err, sql.ErrNoRows) {
		t.Fatal(`!errors.Is(err, sql.ErrNoRows)`)
	}
}

func TestDeepWrapping(t *testing.T) {
	someError := NewStack(
		fmt.Errorf(
			"w1: %w",
			fmt.Errorf("w2: %w", sql.ErrNoRows),
		),
	)

	someError2 := NewStack(sql.ErrNoRows)

	deepWrapped := fmt.Errorf("www: %w", sql.ErrNoRows)

	// Ошибка находится вне зависимости от уровня вложенности
	if !errors.Is(someError, sql.ErrNoRows) {
		t.Fatal(`!errors.Is(someError, sql.ErrNoRows)`)
	}

	// Поиск ошибки идет только в ошибке, целевая ошибка не разворачивается
	if errors.Is(someError, deepWrapped) {
		t.Fatal(`errors.Is(someError, deepWrapped)`)
	}

	// Поиск ошибки идет только в ошибке, целевая ошибка не разворачивается
	if errors.Is(someError2, deepWrapped) {
		t.Fatal(`errors.Is(someError2, deepWrapped)`)
	}
}
