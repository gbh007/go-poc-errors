package entities

import (
	"net/url"

	"github.com/google/uuid"
)

type PageWithHash struct {
	BookID     uuid.UUID
	PageNumber int
	Ext        string
	OriginURL  *url.URL
	Downloaded bool
	FileID     uuid.UUID
	Md5Sum     string
	Sha256Sum  string
	Size       int64
}

type FileHash struct {
	Md5Sum    string
	Sha256Sum string
	Size      int64
}
