package errs

import (
	"database/sql"
	"errors"
	"testing"
)

func recursiveWrap(deep int) error {
	if deep == 1 {
		return NewStackWithCode(sql.ErrNoRows, ErrCode1)
	}

	return NewStack(recursiveWrap(deep - 1))
}

func BenchmarkWrap5(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = recursiveWrap(5)
	}
}

func BenchmarkWrap10(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = recursiveWrap(10)
	}
}

func BenchmarkWrap20(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = recursiveWrap(20)
	}
}

func BenchmarkIs5(b *testing.B) {
	err := recursiveWrap(5)

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		if !errors.Is(err, sql.ErrNoRows) {
			b.FailNow()
		}
	}
}

func BenchmarkIs10(b *testing.B) {
	err := recursiveWrap(10)

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		if !errors.Is(err, sql.ErrNoRows) {
			b.FailNow()
		}
	}
}

func BenchmarkIs20(b *testing.B) {
	err := recursiveWrap(20)

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		if !errors.Is(err, sql.ErrNoRows) {
			b.FailNow()
		}
	}
}
