package table

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
)

type RowScanner func(rows pgx.Rows) error

func (scanner RowScanner) ScanRow(rows pgx.Rows) error {
	return scanner(rows)
}

type RowScannerWrapper[T any] func(data *T) RowScanner

type Table[T any] interface {
	Columns() []string
	ScanAll(ctx context.Context, rows pgx.Rows, data *[]T) error
	ScanRow(ctx context.Context, row pgx.Row, data *T) error
	Select() squirrel.SelectBuilder

	SelectBy(
		ctx context.Context,
		pool *pgxpool.Pool,
		selectModifier func(builder squirrel.SelectBuilder) squirrel.SelectBuilder,
		debugger func(ctx context.Context, query string, args []any),
	) ([]T, error)

	SelectOneBy(
		ctx context.Context,
		pool *pgxpool.Pool,
		selectModifier func(builder squirrel.SelectBuilder) squirrel.SelectBuilder,
		debugger func(ctx context.Context, query string, args []any),
	) (T, error)

	CountOneBy(
		ctx context.Context,
		pool *pgxpool.Pool,
		selectModifier func(builder squirrel.SelectBuilder) squirrel.SelectBuilder,
		debugger func(ctx context.Context, query string, args []any),
	) (int64, error)
}

func NewTable[T any](
	columns []string,
	wrapper RowScannerWrapper[T],
	defaultBuildModifier func(builder squirrel.SelectBuilder) squirrel.SelectBuilder,
) Table[T] {
	return &simpleTableWrapper[T]{
		columns:              columns,
		wrapper:              wrapper,
		defaultBuildModifier: defaultBuildModifier,
	}
}

type simpleTableWrapper[T any] struct {
	columns              []string
	wrapper              RowScannerWrapper[T]
	defaultBuildModifier func(squirrel.SelectBuilder) squirrel.SelectBuilder
}

func (w simpleTableWrapper[T]) Columns() []string {
	return w.columns
}

func (w simpleTableWrapper[T]) ScanAll(ctx context.Context, rows pgx.Rows, data *[]T) error {
	defer rows.Close()

	iter := 0

	for rows.Next() {
		iter++

		var item T

		err := rows.Scan(w.wrapper(&item))
		if err != nil {
			return fmt.Errorf("iteration %d: %w", iter, err)
		}

		*data = append(*data, item)
	}

	return nil
}

func (w simpleTableWrapper[T]) ScanRow(ctx context.Context, row pgx.Row, data *T) error {
	return row.Scan(w.wrapper(data))
}

func (w simpleTableWrapper[T]) Select() squirrel.SelectBuilder {
	return w.defaultBuildModifier(
		squirrel.Select(w.columns...).
			PlaceholderFormat(squirrel.Dollar),
	)
}

func (w simpleTableWrapper[T]) SelectBy(
	ctx context.Context,
	pool *pgxpool.Pool,
	selectModifier func(builder squirrel.SelectBuilder) squirrel.SelectBuilder,
	debugger func(ctx context.Context, query string, args []any),
) ([]T, error) {
	builder := selectModifier(w.Select())

	query, args, err := builder.ToSql()
	if err != nil {
		return nil, fmt.Errorf("build query: %w", err)
	}

	debugger(ctx, query, args)

	rows, err := pool.Query(ctx, query, args...)
	if err != nil {
		return nil, fmt.Errorf("exec query: %w", err)
	}

	out := make([]T, 0)

	err = w.ScanAll(ctx, rows, &out)
	if err != nil {
		return nil, fmt.Errorf("scan all: %w", err)
	}

	return out, nil
}

func (w simpleTableWrapper[T]) SelectOneBy(
	ctx context.Context,
	pool *pgxpool.Pool,
	selectModifier func(builder squirrel.SelectBuilder) squirrel.SelectBuilder,
	debugger func(ctx context.Context, query string, args []any),
) (T, error) {
	builder := selectModifier(w.Select())

	var out T

	query, args, err := builder.ToSql()
	if err != nil {
		return out, fmt.Errorf("build query: %w", err)
	}

	debugger(ctx, query, args)

	row := pool.QueryRow(ctx, query, args...)

	err = w.ScanRow(ctx, row, &out)
	if err != nil {
		return out, fmt.Errorf("scan row: %w", err)
	}

	return out, nil
}

func (w simpleTableWrapper[T]) CountOneBy(
	ctx context.Context,
	pool *pgxpool.Pool,
	selectModifier func(builder squirrel.SelectBuilder) squirrel.SelectBuilder,
	debugger func(ctx context.Context, query string, args []any),
) (int64, error) {
	builder := selectModifier(
		w.defaultBuildModifier(
			squirrel.Select("COUNT(*)").
				PlaceholderFormat(squirrel.Dollar),
		),
	)

	count := sql.NullInt64{}

	query, args, err := builder.ToSql()
	if err != nil {
		return 0, fmt.Errorf("build query: %w", err)
	}

	debugger(ctx, query, args)

	row := pool.QueryRow(ctx, query, args...)

	err = row.Scan(&count)
	if err != nil {
		return 0, fmt.Errorf("scan row: %w", err)
	}

	return count.Int64, nil
}
