package errs

import (
	"runtime"
	"strconv"
)

type Trace interface {
	error

	Trace() string
}

type errorWithTrace struct {
	error

	trace string
}

func WithTrace(err error, skip int) error {
	return &errorWithTrace{
		error: err,
		trace: simpleTrace(skip),
	}
}

func (e *errorWithTrace) Unwrap() error {
	return e.error
}

func (e *errorWithTrace) Trace() string {
	return e.trace
}

func simpleTrace(skip int) string {
	result := ""

	pc := make([]uintptr, 50)
	n := runtime.Callers(skip, pc)
	pc = pc[:n]

	frames := runtime.CallersFrames(pc)

	for {
		frame, more := frames.Next()

		if result != "" {
			result += "\n"
		}

		result += frame.File + ":" + strconv.FormatInt(int64(frame.Line), 10) + " " + frame.Function

		if !more {
			break
		}
	}

	return result
}
