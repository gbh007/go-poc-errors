package errs

import (
	"database/sql"
	"errors"
	"fmt"
	"testing"
)

type testValue struct {
	C int
}

func (testValue) Error() string { return "" }

func TestGeneric(t *testing.T) {
	code1 := SuperWrap(sql.ErrNoRows, testValue{1})
	code2 := SuperWrap(sql.ErrNoRows, testValue{2})
	code2wrap := SuperWrap(fmt.Errorf("some bad: %w", sql.ErrNoRows), testValue{2})
	code1eq := SuperWrap(sql.ErrConnDone, testValue{1})
	code3 := SuperWrap(code2wrap, testValue{3})

	if errors.Is(code1, sql.ErrConnDone) {
		t.Fail()
	}

	if !errors.Is(code1, sql.ErrNoRows) {
		t.Fail()
	}

	if !errors.Is(code2wrap, sql.ErrNoRows) {
		t.Fail()
	}

	var code1Unwrap,
		code2Unwrap,
		code1eqUnwrap,
		code3Unwrap testValue

	if !errors.As(code1, &code1Unwrap) {
		t.FailNow()
	}

	if !errors.As(code2, &code2Unwrap) {
		t.FailNow()
	}

	if !errors.As(code1eq, &code1eqUnwrap) {
		t.FailNow()
	}

	if !errors.As(code3, &code3Unwrap) {
		t.FailNow()
	}

	if code1Unwrap.C != code1eqUnwrap.C {
		t.Fail()
	}

	if code1Unwrap.C == code2Unwrap.C {
		t.Fail()
	}

	if code1Unwrap.C != 1 {
		t.Fail()
	}

	if code2Unwrap.C != 2 {
		t.Fail()
	}

	if code3Unwrap.C != 3 {
		t.Fail()
	}
}
